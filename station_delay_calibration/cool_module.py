#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

""" Cool module containing functions, classes and other useful things """


def greeter():
    """Prints a nice message"""
    print("Hello World!")

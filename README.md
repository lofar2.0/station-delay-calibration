# LOFAR2.0 Station Delay Calibration

![Build status](git.astron.nl/lofar2.0/station-delay-calibration/badges/main/pipeline.svg)
![Test coverage](git.astron.nl/lofar2.0/station-delay-calibration/badges/main/coverage.svg)
<!-- ![Latest release](https://git.astron.nl/templates/python-package/badges/main/release.svg) -->

An example repository of an CI/CD pipeline for building, testing and publishing a python package.

## Installation
```
pip install .
```

## Usage
```python
from station_delay_calibration import cool_module

cool_module.greeter()   # prints "Hello World"
```

## Contributing

To contribute, please create a feature branch and a "Draft" merge request.
Upon completion, the merge request should be marked as ready and a reviewer
should be assigned.

Verify your changes locally and be sure to add tests. Verifying local
changes is done through `tox`.

```pip install tox```

With tox the same jobs as run on the CI/CD pipeline can be ran. These
include unit tests and linting.

```tox```

To automatically apply most suggested linting changes execute:

```tox -e format```

## License
This project is licensed under the Apache License Version 2.0
